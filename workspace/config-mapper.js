
var moMapsConfigs = require('../momapspublic');
var fs = require('fs');
var dict_services = {};


var build_layerServices = function () {
    var layerServices = {};

    //ground layers
    layerServices.ground = {};
    layerServices.ground.layers = ["esriTerrain"];



    //building basemaps
    layerServices.basemaps = [];
    var baseMaps = moMapsConfigs.map.baseMaps;
    var globeBaseMap;
    baseMaps.forEach(element => {
        globeBaseMap = {};
        globeBaseMap.title = element.displayName;
        globeBaseMap.id = element.id;
        globeBaseMap.default = moMapsConfigs.map.primaryMapServiceID == element.id ? true : false;
        globeBaseMap.layers = [];

        if (element.mapServices && element.mapServices.length) {
            element.mapServices.forEach(mapService => {
                globeBaseMap.layers.push(mapService.id);
                globeBaseMap.default = moMapsConfigs.map.primaryMapServiceID == mapService.id ? true : false;
            });
        }

        layerServices.basemaps.push(globeBaseMap);
    });


    //building services
    layerServices.services = [];
    var mapservices = moMapsConfigs.map.mapServices;
    var globeService;
    mapservices.forEach(element => {
        dict_services[element.id] = element;


        globeService = {};
        globeService.id = element.id;
        globeService.title = element.displayName;
        globeService.layerType = element.serviceType.toLowerCase();
        globeService.opacity = element.opacity;
        globeService.maxScale = element.maxScale ? element.maxScale : 0;
        globeService.minScale = element.minScale ? element.minScale : 0;
        if(!element.proxyInfo){
            console.log('no proxy info');
            return;
        }
        globeService.url = 'https://minesonlinemaps.business.qld.gov.au/Geocortex/Essentials/mom-ext/REST/' + element.proxyInfo.address;//element.connectionString.replace('url=', '');
        globeService.visible = element.visible;
        globeService.excludeFromQuery = element.identifiable ? true : false;

        layerServices.services.push(globeService);
    });

    dumpFile('output\\layerServices.json', layerServices);

    //var str_layerServices = JSON.stringify(layerServices);
    //fs.writeFileSync('output\\layerServices.json', str_layerServices);
};



//==============================================================================

var build_toc = function () {
    var toc_roots = [];
    var toc_root;
    var layerList = moMapsConfigs.map.layerList.items;

    if(!fs.existsSync("output\\toc\\")){
        fs.mkdirSync("output\\toc\\");
    }

    layerList.forEach(molayerlist => {
        var tocPath = getTOCPath(molayerlist.name);//molayerlist.name.toLowerCase().replace(' ', '.');
        toc_roots.push(tocPath);

        toc_root = {};
        toc_root.name = molayerlist.name;
        toc_root.expanded = molayerlist.isExpanded;
        toc_root.tocPath = tocPath;
        toc_root.information = '';
        toc_root.children = getChildrenList(molayerlist.items, 'toc-root-' + tocPath); //

        dumpFile('output\\toc\\' + tocPath + '.json', toc_root);
    });
    dumpFile('output\\TOC.json', toc_roots);
}

var getTOCPath = function(name){
    return name.toLowerCase().replace(new RegExp(' ', 'g'), '.');
};

var getChildrenList = function (items, parent_toc_path) {
    if (!items)
        return null;

    var children = [];
    items.forEach(tocItem => {
        var globeTOCItem = {};
        var name;
        var sublayer;
        if (tocItem.name) {
            name = tocItem.name;
        }
        else {
            if(dict_services[tocItem.mapServiceID]){
                sublayer = getSublayer(dict_services[tocItem.mapServiceID].layers, tocItem.layerID);
            }
            
            name = sublayer ? sublayer.displayName : 'NO_SERVICE';
        }

        if (name == 'NO_SERVICE') {
            return;
        }


        var tocPath = getTOCPath(name);
        globeTOCItem.name = name;
        globeTOCItem.tocPath = tocPath;

        if (tocItem.items) {
            globeTOCItem.children = getChildrenList(tocItem.items, parent_toc_path + '-' + tocPath);
        }
        else {
            globeTOCItem.serviceId = tocItem.mapServiceID;
            globeTOCItem.layerId = parseInt(tocItem.layerID);
            globeTOCItem.qspatialUrl = '';
            if (dict_services[tocItem.mapServiceID]) {
                if (!dict_services[tocItem.mapServiceID].sublayers_toc_paths) {
                    dict_services[tocItem.mapServiceID].sublayers_toc_paths = [];
                }
                dict_services[tocItem.mapServiceID].sublayers_toc_paths.push(
                    {
                        topic_path: parent_toc_path + '-' + tocPath,
                        themeSettings: sublayer ? sublayer.themeSettings : null,
                        layerID: sublayer.id
                    }
                );
            }
        }

        children.push(globeTOCItem);
    });

    return children;
};

var getSublayer = function (layers, id) {
    var count = layers.length;
    for (var i = 0; i < count; i++) {
        if (layers[i].id == id)
            return layers[i];
    }
    return null;
};

//==============================================================================
var dict_themes = [];
var build_layers_themes_dict = function(){
    var service;
    for(var key in dict_services){
        service = dict_services[key];
        for(var sublayerIndex in service.sublayers_toc_paths){
            sublayer = service.sublayers_toc_paths[sublayerIndex];
            
            for(var themeSettingIndx in sublayer.themeSettings){
                themeSetting = sublayer.themeSettings[themeSettingIndx];

                if(!dict_themes[themeSetting.themeID]){
                    dict_themes[themeSetting.themeID] = {};
                    dict_themes[themeSetting.themeID].layers = [];
                    dict_themes[themeSetting.themeID].invisibleLayers = [];
                }

                dict_themes[themeSetting.themeID].layers.push(sublayer.topic_path);
                if(!themeSetting.visible){
                    dict_themes[themeSetting.themeID].invisibleLayers.push(sublayer.topic_path);
                }
            }

        }
    }
};

var build_layers_themes = function(){
    var layerThemes = moMapsConfigs.map.layerThemes.items;
    var topics_list = [];
    var topic;
    layerThemes.forEach(moTheme => {
        topic = {};
        topic.title = moTheme.displayName;
        topic.globeId = moTheme.id;
        topic.selected = moMapsConfigs.map.layerThemes.startupThemeID == moTheme.id ? true : false;
        topic.draft = false;
        topic.archived = false;
        topic.descriptionImage = '';
        topic.description = '';
        topic.layers = dict_themes[moTheme.id].layers;
        topic.invisibleLayers = dict_themes[moTheme.id].invisibleLayers;
        topic.tags = [];

        topics_list.push(topic);
    })

    dumpFile('output\\topics.json', topics_list);
};

//==============================================================================

var dumpFile = function (path, data) {
    var str_data = JSON.stringify(data);
    fs.writeFileSync(path, str_data);
};

build_layerServices();

build_toc();

build_layers_themes_dict();
build_layers_themes();
console.log('ok');
