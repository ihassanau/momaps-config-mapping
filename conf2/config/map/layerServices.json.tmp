{
  "basemaps": [
    {
      "title": "DNRM Basemap",
      "id": "DnrmBasemap",
      "layers": [
        "esriImagery"
      ],
      "reference": [
        "esriLabels"
      ],
      "default": false,
      "basemapType": "custom"
    },
    {
      "name": "hybrid",
      "ground": "world-elevation",
      "default": true,
      "basemapType": "esri"
    }
  ],
  "ground": {
    "layers": [
      "esriTerrain"
    ]
  },
  "services": [
    {
      "id": "DnrmTerrain",
      "title": "DNRM Terrain",
      "layerType": "elevation",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Elevation/QldDem/ImageServer",
      "visible": true,
      "excludeFromLegend": true
    },
    {
      "id": "esriImagery",
      "title": "Esri World Imagery Service",
      "layerType": "tiled",
      "layerFloat": -200,
      "opacity": 1,
      "url": "//services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer",
      "visible": true,
      "excludeFromLegend": true
    },
    {
      "id": "esriLabels",
      "title": "Esri World Labels",
      "layerType": "tiled",
      "opacity": 1,
      "url": "//services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer",
      "visible": true,
      "maxScale": 20000001,
      "excludeFromLegend": true
    },
    {
      "id": "places",
      "title": "Places",
      "layerType": "dynamic",
      "layerFloat": 300,
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/Location/Places/MapServer",
      "visible": true,
      "imageFormat": "png32",
      "excludeFromQuery": true,
      "minScale": 20000002,
      "maxScale": 0,
      "excludeFromLegend": true,
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "aca",
      "title": "Aquatic Conservation Assessments",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Biota/AquaticConservationAssessments/MapServer",
      "visible": false,
      "layerFloat": 400,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "biocorr",
      "title": "Statewide Biodiversity Corridors",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Biota/StatewideBiodiversityCorridors/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "bpa",
      "title": "Biodiversity Planning Assessments",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Biota/BiodiversityPlanningAssessments/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "gde",
      "title": "Groundwater Dependent Ecosystems",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Biota/GroundwaterDependentEcosystems/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "remap",
      "title": "Regional ecosystem mapping",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Biota/RegionalEcosystemMapping/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "vma",
      "title": "Vegetation management",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Biota/VegetationManagement/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680,
      "sublayerIdsToExcludeFromIdentify": [
        110
      ]
    },
    {
      "id": "absbound",
      "title": "Australian Bureau of Statistics census boundaries",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/QldGlobe/CensusBoundaries/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "adminbdyframe",
      "title": "Admin Boundaries Framework",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Boundaries/AdminBoundariesFramework/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "adminbdy",
      "title": "Administrative Boundaries",
      "layerType": "dynamic",
      "layerFloat": 300,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Boundaries/AdministrativeBoundaries/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "mineadminareas",
      "title": "Mining Boundaries - Block grids",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Boundaries/MiningAdministrativeAreas/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "nswAdmin",
      "title": "NSW administrative boundaries",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Administrative_Boundaries/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "DebImg",
      "title": "Cyclone Debbie 2017 Imagery ",
      "layerType": "image",
      "opacity": 1,
      "url": "@@imageryHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_Imagery/ImageServer",
      "visible": false,
      "excludeFromLegend": true,
      "copyright": "includes material &copy; 21AT &copy; Earth-i"
    },
    {
      "id": "DebImg2",
      "title": "Cyclone Debbie 2017 Imagery Q4 2016",
      "layerType": "image",
      "opacity": 1,
      "url": "@@imageryHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_Imagery_BaselineQ416_2pt4m/ImageServer",
      "visible": false,
      "excludeFromLegend": true,
      "copyright": "includes material &copy; Planet Labs Netherlands B.V. 2016, reproduced under licence from Planet and Geoplex, all rights reserved"
    },
    {
      "id": "DebImg3",
      "title": "Cyclone Debbie 2017 Imagery Post 3m",
      "layerType": "image",
      "opacity": 1,
      "url": "@@imageryHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_Imagery_Post_3m/ImageServer",
      "visible": false,
      "excludeFromLegend": true,
      "copyright": "&copy; Planet Labs Netherlands B.V. 2016, reproduced under licence from Planet and Geoplex, all rights reserved"
    },
    {
      "id": "DebImg4",
      "title": "Cyclone Debbie 2017 Imagery Post 5m",
      "layerType": "image",
      "opacity": 1,
      "url": "@@imageryHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_Imagery_Post_5m/ImageServer",
      "visible": false,
      "excludeFromLegend": true,
      "copyright": "&copy; Planet Labs Netherlands B.V. 2016, reproduced under licence from Planet and Geoplex, all rights reserved"
    },
    {
      "id": "DebImg5",
      "title": "Cyclone Debbie 2017 Imagery Post 10cm",
      "layerType": "image",
      "opacity": 1,
      "url": "@@imageryHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_Imagery_Post_10cm/ImageServer",
      "visible": false,
      "excludeFromLegend": true
    },
    {
      "id": "DebBom",
      "title": "Cyclone Debbie 2017 track",
      "layerType": "dynamic",
      "opacity": 1,
      "layerFloat": 150,
      "url": "@@mapHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_BOM/MapServer",
      "visible": false
    },
    {
      "id": "DebRes",
      "title": "Tropical cyclone Debbie response",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/EventsIncidents/CycloneDebbie2017_Response/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "kp",
      "title": "Koala plan",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/KoalaPlan/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "nrmip",
      "title": "Nrm investment program",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/NrmInvestmentPrograms/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "surat",
      "title": "Surat cumulative management area",
      "layerType": "dynamic",
      "layerFloat": 300,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/SuratCumulativeMgmtArea/MapServer",
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "mpark",
      "title": "Marine protected areas",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/ParksMarineProtectedAreas/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "tpark",
      "title": "Terrestial protected areas",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/ParksTerrestrialProtectedAreas/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "wetp",
      "title": "Wetland protection",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/WetlandProtection/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "slats",
      "title": "State-wide landcover and trees study",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/VegView/SLATS/MapServer",
      "imageFormat": "png32",
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "nswImg",
      "title": "NSW imagery",
      "layerType": "tiled",
      "layerFloat": -200,
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer",
      "visible": false,
      "excludeFromLegend": true
    },
    {
      "id": "DnrmImagery",
      "title": "DNRM Imagery",
      "layerType": "tiled",
      "layerFloat": -100,
      "opacity": 1,
      "url": "//gisservices.information.qld.gov.au/arcgis/rest/services/Imagery/QldBase_QgovSispUsers/ImageServer",
      "visible": false,
      "copyright": "includes material &copy; CNES reproduced under licence from Airbus DS, all rights reserved, &copy; 21AT &copy; Earth-i, all rights reserved, 2017",
      "excludeFromLegend": true
    },
    {
      "id": "dnrmImageryDynamic",
      "title": "DNRM Imagery (Dynamic)",
      "layerType": "dynamic",
      "layerFloat": -50,
      "opacity": 1,
      "url": "//gisservices3.information.qld.gov.au/arcgis/rest/services/Imagery/QldBase_QgovSispUsers/MapServer",
      "visible": false,
      "excludeFromLegend": true,
      "excludeFromQuery": true,
      "minScale": 1128,
      "maxScale": 0,
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "sisp",
      "title": "SISP Imagery (Dynamic)",
      "layerType": "image",
      "opacity": 1,
      "url": "//psa.dnrm.esriaustraliaonline.com.au/arcgis/rest/services/AARNET_SISP_Imagery/Central_Qld_Coastal_2013_50cm_WM/ImageServer",
      "visible": false,
      "excludeFromLegend": true,
      "maxImageHeight": 4100,
      "maxImageWidth": 15000
    },
    {
      "id": "earthi2016",
      "title": "Earth-i 2016",
      "layerType": "image",
      "layerFloat": -100,
      "opacity": 1,
      "url": "//psa.dnrm.esriaustraliaonline.com.au/arcgis/rest/services/EarthI2016/ImageServer",
      "visible": false,
      "excludeFromLegend": true,
      "maxImageHeight": 4100,
      "maxImageWidth": 15000
    },
    {
      "id": "footprint",
      "title": "DNRM Imagery (Dynamic)",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//gisservices3.information.qld.gov.au/arcgis/rest/services/Imagery/QldBase_QgovSispUsers/MapServer",
      "visible": false,
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "citiestowns",
      "title": "Cities and towns",
      "layerType": "tiled",
      "cacheBusting": true,
      "layerFloat": 500,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Location/CitiesAndTowns/MapServer",
      "visible": false,
      "minScale": 35000000,
      "maxScale": 9027.977761,
      "imageformat": 32
    },
    {
      "id": "cadastre",
      "title": "Cadastre",
      "layerType": "dynamic",
      "layerFloat": 200,
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/QldGlobe/CadastralFramework/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "sublayerIdsToExcludeFromIdentify": [
        2,
        6,
        7,
        22,
        23,
        24,
        25,
        26
      ]
    },
    {
      "id": "roadscache",
      "title": "Roads Cache",
      "layerType": "tiled",
      "cacheBusting": true,
      "layerFloat": 100,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Transportation/RoadsCache/MapServer",
      "visible": false,
      "excludeFromQuery": true,
      "imageFormat": "png32",
      "minScale": 9244648.868618,
      "maxScale": 4513.988705
    },
    {
      "id": "roads",
      "title": "Roads",
      "layerType": "dynamic",
      "layerFloat": 100,
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/Transportation/Roads/MapServer",
      "visible": false,
      "sublayerIdsToExcludeFromIdentify": [
        0
      ],
      "imageFormat": "png32",
      "minScale": 4514,
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "transport",
      "title": "Transport",
      "layerType": "dynamic",
      "layerFloat": 100,
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/Transportation/OtherTransport/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "stateroad",
      "title": "State road information",
      "layerType": "dynamic",
      "layerFloat": 100,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Transportation/StateRoadInformation/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "watercoursescache",
      "title": "Watercourses and bodies (Cache)",
      "layerType": "tiled",
      "cacheBusting": true,
      "layerFloat": 200,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/InlandWaters/WaterCoursesAndBodiesCache/MapServer",
      "visible": false,
      "minScale": 2311162.217155,
      "maxScale": 3001,
      "imageFormat": "png32"
    },
    {
      "id": "watercourses",
      "title": "Watercourses and bodies",
      "layerType": "dynamic",
      "layerFloat": 200,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/InlandWaters/WaterCoursesAndBodies/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "minScale": 3002,
      "sublayerIdsToExcludeFromIdentify": [
        15
      ],
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "drainage",
      "title": "Drainage",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/InlandWaters/DrainageBasins/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "mineloc",
      "title": "Mine locations",
      "layerType": "dynamic",
      "opacity": 1,
      "layerFloat": 100,
      "url": "@@terrainHost/arcgis/rest/services/Economy/MineLocation/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "minepermitscurrent",
      "title": "Mining permits current",
      "layerType": "dynamic",
      "opacity": 1,
      "layerFloat": 50,
      "url": "@@mapHost/arcgis/rest/services/Economy/MinesPermitsCurrent/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "historical",
      "title": "historical permits",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Economy/MinesPermitsHistoric/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "keyResource",
      "title": "Key Resource",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/MiningResources/MapServer",
      "visible": false,
      "layerFloat": 200,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "structFrame",
      "title": "Structural Framework",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/StructuralFramework/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "geoObs",
      "title": "Geology Observations",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/GeologyObservations/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 768
    },
    {
      "id": "geoDetail",
      "title": "Geology Detailed",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/GeologyDetailed/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "geoRegional",
      "title": "Geology Regional",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/GeologyRegional/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "geoState",
      "title": "Geology State",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/GeologyState/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "geophysics",
      "title": "Geophysical Survey",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/Geophysics/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "contourscache",
      "title": "Contours (Cached)",
      "layerType": "tiled",
      "cacheBusting": true,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Elevation/ContoursCache/MapServer",
      "excludeFromQuery": true,
      "visible": false,
      "minScale": 288895.277144,
      "maxScale": 9027.977411,
      "imageFormat": "png32"
    },
    {
      "id": "contours",
      "title": "Contours (Dynamic)",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Elevation/Contours10m/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "boreholes",
      "title": "Boreholes",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/GeoscientificInformation/Boreholes/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "watermonitoring",
      "title": "Water monitoring",
      "layerType": "dynamic",
      "opacity": 1,
      "layerFloat": 200,
      "url": "@@mapHost/arcgis/rest/services/InlandWaters/GroundAndSurfaceWaterMonitoring/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "watermanagement",
      "title": "Water Management",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/InlandWaters/WaterManagement/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "wim",
      "title": "Watercourse Identification Map",
      "layerType": "dynamic",
      "layerFloat": 500,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/InlandWaters/WatercourseIdentificationMap/MapServer",
      "visible": false,
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "coastwater",
      "title": "Coastal Water",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Oceans/CoastalWaters/MapServer",
      "visible": false,
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "aglandaudit",
      "title": "Agricultural Land Audit",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Farming/AgriculturalLandAudit/MapServer",
      "visible": false,
      "layerFloat": 500,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "stockroute",
      "title": "Stock routes Queensland",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Farming/StockRoutesQld/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "glm",
      "title": "Grazing land management",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Environment/LandTypes/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "sownp",
      "title": "Sown pasture current and potential",
      "layerType": "tiled",
      "cacheBusting": true,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Farming/PastureSownCurrentAndPotential/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "pastcurr",
      "title": "Pasture production current",
      "layerType": "tiled",
      "cacheBusting": true,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Farming/PastureProductionCurrent/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "pastpot",
      "title": "Pasture production potential",
      "layerType": "tiled",
      "cacheBusting": true,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Farming/PastureProductionPotential/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "scl",
      "title": "Strategic cropping land",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Farming/StrategicCroppingLand/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "qtopo",
      "title": "Queensland topographic map",
      "layerType": "tiled",
      "cacheBusting": true,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Basemaps/QldMap_Topo/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680,
      "minScale": "7.3957190948944E7",
      "maxScale": 1128.497176
    },
    {
      "id": "resland",
      "title": "Residential land supply",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/PlanningCadastre/ResidentialLandSupply/MapServer",
      "visible": false,
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "landuse",
      "title": "Land use",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/PlanningCadastre/LandUse/MapServer",
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "regplan",
      "title": "Regional plans",
      "layerType": "dynamic",
      "opacity": 1,
      "layerFloat": 300,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/QldGlobe/RegionalPlans/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "priordev",
      "title": "Priority development areas",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/PlanningCadastre/PriorityDevelopmentAreas/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "statedevarea",
      "title": "State development areas",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/PlanningCadastre/StateDevelopmentAreas/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "coordproj",
      "title": "Coordinated projects",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/PlanningCadastre/CoordinatedProjects/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "regionint",
      "title": "Areas of regional interest",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/PlanningCadastre/AreasOfRegionalInterest/MapServer",
      "visible": false,
      "imageFormat": "png32"
    },
    {
      "id": "mount",
      "title": "Mountain ranges",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Elevation/MountainRanges/MapServer",
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "survey",
      "title": "Survey Control",
      "layerType": "dynamic",
      "layerFloat": 500,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Location/SurveyControl/MapServer",
      "imageFormat": "png32",
      "maxImageHeight": 4320,
      "maxImageWidth": 7680
    },
    {
      "id": "nntt",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/0"
    },
    {
      "id": "nntt1",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/1"
    },
    {
      "id": "nntt2",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/2"
    },
    {
      "id": "nntt3",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/3"
    },
    {
      "id": "nntt4",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/4"
    },
    {
      "id": "nntt5",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/5"
    },
    {
      "id": "nntt6",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/6"
    },
    {
      "id": "nntt7",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/7"
    },
    {
      "id": "nntt8",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/8"
    },
    {
      "id": "nntt9",
      "title": "National Native Title Tribunal",
      "layerType": "feature",
      "opacity": 1,
      "url": "//services2.arcgis.com/rzk7fNEt0xoEp3cX/arcgis/rest/services/NNTT_Custodial_AGOL/FeatureServer/9"
    },
    {
      "id": "soils",
      "title": "Soil and Land Resources",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/QldGlobe/SoilAndLandResource/MapServer",
      "layerFloat": 150,
      "imageFormat": "png32",
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "landvalue",
      "title": "Land valuation",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/QldGlobe/Valuations/MapServer",
      "imageFormat": "png32",
      "sublayerIdsToExcludeFromIdentify": [
        2,
        3,
        4
      ]
    },
    {
      "id": "school",
      "title": "Schools and school catchments",
      "layerType": "dynamic",
      "layerFloat": 500,
      "opacity": 1,
      "url": "@@mapHost/arcgis/rest/services/Society/SchoolsAndSchoolCatchments/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "atdw",
      "title": "Australian tourism data warehouse",
      "layerType": "dynamic",
      "layerFloat": 350,
      "opacity": 1,
      "url": "//gisservices4.information.qld.gov.au/arcgis/rest/services/Economy/Tourism/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "pls",
      "title": "PLS",
      "layerType": "dynamic",
      "opacity": 0.6,
      "url": "@@mapHost/arcgis/rest/services/PLS/Addons/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "2012floods",
      "title": "2012 flood lines",
      "layerType": "dynamic",
      "opacity": 15,
      "url": "@@mapHost/arcgis/rest/services/Historic_Flood_Lines/2012_Interim_Flood_Lines/MapServer",
      "layerFloat": 50,
      "visible": false
    },
    {
      "id": "2012floodimage",
      "title": "2012 flood imagery",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "@@imageryHost/arcgis/rest/services/Historic_Imagery/2012_Flood_Imagery/MapServer",
      "visible": false,
      "group": "dynamic",
      "excludeFromLegend": true
    },
    {
      "id": "2010floodimage",
      "title": "2010/11 flood imagery",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//psa.dnrm.esriaustraliaonline.com.au/arcgis/rest/services/Historic_Imagery/2010_to_2011_Flood_and_Cyclone/MapServer",
      "visible": false,
      "excludeFromLegend": true
    },
    {
      "id": "2010floodline",
      "title": "2010/11 flood line",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//psa.dnrm.esriaustraliaonline.com.au/arcgis/rest/services/Historic_Flood_Lines/2010_to_2011_Interim_Flood_Lines/MapServer",
      "visible": false,
      "layerFloat": 50,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "rapidhazardassess",
      "title": "Rapid Hazard Assessment Map Series",
      "layerType": "dynamic",
      "opacity": 0.7,
      "url": "//dnrm-ags.esriaustraliaonline.com.au/arcgis/rest/services/Floodplain_Extent_(QFAO)_Level_1/Rapid_Hazard_Assessment/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "extremebasinevents",
      "title": "Extreme Basin Events",
      "layerType": "dynamic",
      "opacity": 0.7,
      "url": "//dnrm-ags.esriaustraliaonline.com.au/arcgis/rest/services/Basin_Level_Flood_Modelling/Basin_Extreme_Events/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "AEPbasinevents",
      "title": "AEP Basin Events",
      "layerType": "dynamic",
      "opacity": 0.7,
      "url": "//dnrm-ags.esriaustraliaonline.com.au/arcgis/rest/services/Basin_Level_Flood_Modelling/Basin_1_percent_AEP/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "planLab",
      "title": "Planet labs",
      "layerType": "image",
      "opacity": 1,
      "url": "@@terrainHost/arcgis/rest/services/VegEDS/PlanetLabs/ImageServer",
      "visible": false,
      "excludeFromLegend": true
    },
    {
      "id": "lndSat",
      "title": "Landsat8 - 2015",
      "layerType": "image",
      "opacity": 1,
      "url": "@@terrainHost/arcgis/rest/services/Imagery/QldLandsat8_2015/ImageServer",
      "visible": false,
      "excludeFromLegend": true
    },
    {
      "id": "nswImgD",
      "title": "NSW imagery dates",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery_Dates/MapServer",
      "layerFloat": 100,
      "visible": false
    },
    {
      "id": "nswTopo",
      "title": "NSW topo map",
      "layerType": "tiled",
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Topo_Map/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "nswSurvey",
      "title": "NSW survey marks",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Survey_Mark/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "nswCad",
      "title": "NSW Cadastre",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Cadastre/MapServer",
      "imageFormat": "png32",
      "sublayerIdsToExcludeFromIdentify": [
        1,
        2,
        3
      ]
    },
    {
      "id": "nswPOI",
      "title": "NSW points of interest",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_POI/MapServer",
      "imageFormat": "png32"
    },
    {
      "id": "earthquakes",
      "title": "Earthquake Hazard",
      "layerType": "dynamic",
      "opacity": 0.5,
      "url": "//www.ga.gov.au/gis/rest/services/hazards/EarthquakeHazard/MapServer",
      "visible": false
    },
    {
      "id": "gasinfrastructure",
      "title": "Oil and Gas Infrastructure",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//services.ga.gov.au/gis/rest/services/Oil_Gas_Infrastructure/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "electrical",
      "title": "Electrical Infrastructure",
      "layerType": "dynamic",
      "opacity": 1,
      "url": "//services.ga.gov.au/gis/rest/services/Electricity_Infrastructure/MapServer",
      "visible": false,
      "maxImageHeight": 4096,
      "maxImageWidth": 4096
    },
    {
      "id": "brat",
      "title": "BRAT",
      "layerType": "vectortiles",
      "opacity": 1,
      "url": "//tiles.arcgis.com/tiles/HrMiNYsSqqPpLTDE/arcgis/rest/services/BRAT/VectorTileServer",
      "visible": true
    },
    {
      "id": "earthdata",
      "title": "NASA Earthdata",
      "layerType": "wmts",
      "opacity": 0.5,
      "url": "//gibs-c.earthdata.nasa.gov/wmts/epsg3857/best",
      "visible": false,
      "activeLayer": "SRTM_Color_Index"
    },
    {
      "id": "nativeTitles",
      "title": "Australian Surface Hydrology",
      "layerType": "wms",
      "opacity": 1,
      "url": "http://services.ga.gov.au/site_1/services/Surface_Hydrology_WM/MapServer/WMSServer",
      "visible": false,
      "sublayers": "Basins"
    }
  ]
}