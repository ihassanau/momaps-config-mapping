{
  "name": "Biota (Flora & Fauna)",
  "expanded": false,
  "tocPath": "biota",
  "information": "<p>The biota theme provides access to information about naturally occurring flora and fauna. Information available includes&#58 Vegetation management information.</p>",
  "children": [
    {
      "name": "Biogeographic region",
      "tocPath": "bioreg",
      "serviceId": "adminbdyframe",
      "layerId": 2,
      "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Biogeographic%20regions%20-%20Queensland%22"
    },
    {
      "name": "Biogeographic subregion",
      "tocPath": "biosubreg",
      "serviceId": "adminbdyframe",
      "layerId": 3,
      "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Biogeographic%20regions%20-%20Queensland%22"
    },
    {
      "name": "Vegetation management information",
      "tocPath": "vmi",
      "information": "<p>This topic contains mapping products that assist you to work through the vegetation management framework. It details areas of regulation and outlines rules and values that must be considered when clearing native vegetation.</p> <p>It also includes mapping products to assist with determining land suitability.</p> <p>Read more about <a href=\"https://www.qld.gov.au/environment/land/vegetation/management/\" target=\"blank\">vegetation management in Queensland</a></p> <p>For more information about vegetation management or the vegetation management topic, please contact us by <a href=\"mailto:vegetation@dnrm.qld.gov.au?subject=Queensland Globe - Vegetation management enquiry\">email</a> or call 135VEG (13 58 34).</p> <h2>Vegetation management layers</h2> <p>The vegetation management topic is comprised of a number of layers extracted from various sources.</p> <h3>Regulated vegetation management map</h3> <p>The regulated vegetation management map shows areas as:</p> <ul><li>Category A (red) - areas subject to compliance notices, offsets and voluntary declarations</li> <li>Category B (dark blue) - areas of remnant vegetation with no differentiation between conservation status</li> <li>Category C (light blue) - areas of high-value regrowth on leasehold land for agriculture and grazing purposes with no differentiation between conservation status</li> <li>Category R (yellow) - regrowth watercourse areas on freehold land, indigenous land and leasehold land for agriculture and grazing purposes within the priority reef catchment area of the Wet Tropics, Burdekin and Mackay Whitsunday areas</li></ul> <p>Non-regulated areas of vegetation are shown as:</p> <ul><li>Category X (white) - areas which are non-remnant, not regulated regrowth and not subject to compliance notices, offsets or voluntary declaration</li></ul> <h3>Vegetation management supporting map</h3> <p>Incorporates a number of mapping layers including:</p> <ul><li>Regional ecosystem and remnant mapping</li> <li>Wetlands</li> <li>Watercourses and</li> <li>Essential habitat</li></ul> <p>These layers will assist you to determine:</p> <ul><li>determine whether the proposed clearing meets an exempt purpose</li> <li>understand the requirements of a self-assessable vegetation clearing codes if your clearing is not exempt and you are clearing under a self-assessable code or</li> <li>address the performance requirements of the State Development Assessment Provisions if your proposed clearing requires a development approval</li></ul> <h3>Land suitability overview map</h3> <p>The land suitability overview map assists with identifying the Land Suitability category (2, 3 or 4) under the high-value and irrigated high-value agriculture clearing purpose. This layer can be used to identify land covered by:</p> <ul><li>The land suitability overview map can be used to identify land covered by&#58 Land suitability mapping 1&#58 100,000 scale or better (Category 2 or 3*)</li> <li>Land resource mapping greater than 100,000 scale (Category 4)</li> <li>No mapping available (Category 4)</li></ul> <p>* Category 3 applies to applications where there is some land resource mapping or information available however it either does not cover the entire area, or the land suitability mapping and information does not identify the land as suitable for the proposed crop and management systems.</P> <p>Soil and land resource projects vary considerably in their age, scale, purpose, and method of production.</p> <p>These maps are based on data extracted from the Soil and Land Information System (SALI) supplied by the Queensland Government on 09/07/2015.</p> <p>To identify the specific survey and data for your area of interest, zoom out and turn on the category group layer that applies to your area of interest.</p> <p>For information on high-value agriculture clearing or irrigated high-value agriculture clearing policy or the specific assessment requirements go to <a href=\"https://www.qld.gov.au/environment/land/vegetation/agriculture/\" target=\"blank\">Queensland Government website</a>.</p> <p>For queries about the soils data <a href=\"mailto:soils@qld.gov.au?subject=Queensland Globe - Vegetation management enquiry\">email</a></p> <h3>Land suitability category 2, 3 or 4</h3> <p>These layers can be used to identify an area as a category 2, 3 or 4 Land suitability area for high-value agriculture or irrigated high-value agriculture.</p> <p>These maps are based on data extracted from the Soil and Land Information System (SALI) supplied by the Queensland Government on 09/07/2015.</p> <p>For more information about the type of surveys in these layers go to the Queensland Government soils website and view the pages on <a href=\"http://www.qld.gov.au/environment/land/soil/soil-data/survey-types/\" target=\"blank\">survey type</a> and <a href=\"http://www.qld.gov.au/environment/land/soil/soil-data/land-evaluation/\" target=\"blank\">land evaluation schemes</a>.</p>",
      "children": [
        {
          "name": "Regulated vegetation management map",
          "tocPath": "rvmp",
          "serviceId": "vma",
          "layerId": 0,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Regulated vegetation management map%22"
        },
        {
          "name": "Vegetation management supporting map",
          "tocPath": "vmsm",
          "serviceId": "vma",
          "layerId": 2,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Regional ecosystem and remnant map%22"
        },
        {
          "name": "Property map of assessable vegetation (PMAV)",
          "tocPath": "pmav",
          "serviceId": "vma",
          "layerId": 14,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Vegetation Management Act property maps of assessable vegetation%22"
        },
        {
          "name": "Vegetation management wetlands map",
          "tocPath": "wetlands",
          "serviceId": "vma",
          "layerId": 4,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Vegetation management wetlands map%22"
        },
        {
          "name": "Essential habitat map",
          "tocPath": "essentialhabitat",
          "serviceId": "vma",
          "layerId": 5,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Essential habitat map%22"
        },
        {
          "name": "Vegetation management watercourse/drainage",
          "tocPath": "drainage",
          "children": [
            {
              "name": "Vegetation management watercourse/drainage - 1:100,000 and 1:250,000",
              "tocPath": "vmwt100",
              "serviceId": "vma",
              "layerId": 7,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Vegetation management watercourse and drainage feature map (1:100000 and 1:250000)%22"
            },
            {
              "name": "Vegetation management watercourse/drainage - 1:25,000",
              "tocPath": "vmwt25",
              "serviceId": "vma",
              "layerId": 8,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Vegetation management watercourse and drainage feature map (1:25000)%22"
            }
          ]
        },
        {
          "name": "High-value agriculture (HVA)",
          "tocPath": "hva",
          "children": [
            {
              "name": "HVA land suitability overview map",
              "tocPath": "hvalandsuit",
              "serviceId": "vma",
              "layerId": 10
            },
            {
              "name": "HVA category 2 or 3 land suitability",
              "tocPath": "hvacat2",
              "serviceId": "vma",
              "layerId": 11
            },
            {
              "name": "HVA category 4 land suitability",
              "tocPath": "hvacat4",
              "serviceId": "vma",
              "layerId": 12
            }
          ]
        }
      ]
    }
  ]
}