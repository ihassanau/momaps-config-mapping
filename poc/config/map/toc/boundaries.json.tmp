{
  "name": "Boundaries",
  "information": "\nThe boundaries theme provides access to the collection of legislative, regulatory, political, statistical, electoral, maritime and other general \nadministrative boundaries sourced from local and state boundary datasets.  Information available includes&#58 Australian census data, commercial fishery reporting grids, electoral boundaries, \nstate border, local government boundaries, postcodes and mining administrative boundaries.\n \n",
  "expanded": false,
  "tocPath": "boundaries",
  "children": [
    {
      "name": "Australian census data",
      "tocPath": "census",
      "information": "The census is the largest form of information gathering conduct in Australia - it tells us about our way of life and helps us plan for the future, for <a href = \"http://www.abs.gov.au/websitedbs/censushome.nsf/home/Census?opendocument&ref=topBar\" target =\"_blankd\">more information</a>.",
      "children": [
        {
          "name": "Statistical area level 4 (SA4)",
          "tocPath": "census4",
          "serviceId": "absbound",
          "layerId": 0,
          "qspatialUrl": "http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016?OpenDocument"
        },
        {
          "name": "Statistical area level 3 (SA3)",
          "tocPath": "census3",
          "serviceId": "absbound",
          "layerId": 1,
          "qspatialUrl": "http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016?OpenDocument"
        },
        {
          "name": "Statistical area level 2 (SA2)",
          "tocPath": "census2",
          "serviceId": "absbound",
          "layerId": 2,
          "qspatialUrl": "http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016?OpenDocument"
        },
        {
          "name": "Statistical area level 1 (SA1)",
          "tocPath": "census1",
          "serviceId": "absbound",
          "layerId": 3,
          "qspatialUrl": "http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016?OpenDocument"
        },
        {
          "name": "Mesh blocks",
          "tocPath": "censusmesh",
          "serviceId": "absbound",
          "layerId": 4,
          "qspatialUrl": "http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016?OpenDocument"
        }
      ]
    },
    {
      "name": "Commercial fishery reporting grids",
      "tocPath": "comfish",
      "children": [
        {
          "name": "Commercial fishery 30 minute reporting grid",
          "tocPath": "comfish30",
          "serviceId": "adminbdyframe",
          "layerId": 89,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Queensland%20commercial%20fishery%2030%20minute%20reporting%20grid%22"
        },
        {
          "name": "Commercial fishery 6 minute reporting grid",
          "tocPath": "comfish6",
          "serviceId": "adminbdyframe",
          "layerId": 88,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Queensland%20commercial%20fishery%206%20minute%20reporting%20grid%22"
        }
      ]
    },
    {
      "name": "Electoral",
      "tocPath": "electoral",
      "children": [
        {
          "name": "Federal electorate",
          "tocPath": "fedelectoral",
          "serviceId": "adminbdy",
          "layerId": 4,
          "qspatialUrl": "http://www.aec.gov.au/Electorates/gis/index.htm"
        },
        {
          "name": "State electorate",
          "tocPath": "stateelec",
          "children": [
            {
              "name": "State electorate",
              "tocPath": "stateeleccur",
              "serviceId": "adminbdy",
              "layerId": 5,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22State%20electoral%20boundaries%20Queensland%22"
            },
            {
              "name": "State electorate future",
              "tocPath": "stateelecfut",
              "serviceId": "adminbdy",
              "layerId": 9
            }
          ]
        },
        {
          "name": "Local government division",
          "tocPath": "lgaelec",
          "serviceId": "adminbdy",
          "layerId": 7
        },
        {
          "name": "Brisbane City Council ward",
          "tocPath": "lgaelecbcc",
          "serviceId": "adminbdy",
          "layerId": 8
        }
      ]
    },
    {
      "name": "Local government",
      "tocPath": "localgovt",
      "serviceId": "adminbdy",
      "layerId": 1,
      "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Local%20government%20area%20boundaries%20-%20Queensland%22"
    },
    {
      "name": "Locality",
      "tocPath": "localbdy",
      "serviceId": "adminbdy",
      "layerId": 2,
      "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Locality%20boundaries%20-%20Queensland%22"
    },
    {
      "name": "Mining",
      "tocPath": "mining",
      "children": [
        {
          "name": "Blocks and sub-blocks",
          "tocPath": "minblocksbu",
          "children": [
            {
              "name": "Block identification map index",
              "tocPath": "minbimi",
              "serviceId": "mineadminareas",
              "layerId": 1,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Block%20identification%20map%20key%20sheets%20-%20geographic%20grid%20pattern%20-%20Queensland%22"
            },
            {
              "name": "Mining blocks",
              "tocPath": "minblock",
              "serviceId": "mineadminareas",
              "layerId": 2,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Block%20grids%20-%20geographic%20grid%20pattern%20-%20Queensland%22"
            },
            {
              "name": "Mining sub-blocks",
              "tocPath": "minsubblock",
              "serviceId": "mineadminareas",
              "layerId": 3,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Subblock%20grids%20-%20geographic%20grid%20pattern%20-%20Queensland%22"
            }
          ]
        },
        {
          "name": "Mining districts",
          "tocPath": "mindistrict",
          "serviceId": "mineadminareas",
          "layerId": 4,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Mining%20regions%20and%20districts%20-%20Queensland%22"
        },
        {
          "name": "Mining regions",
          "tocPath": "minregion",
          "serviceId": "mineadminareas",
          "layerId": 5,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Mining%20regions%20and%20districts%20-%20Queensland%22"
        },
        {
          "name": "Unavailable lands",
          "tocPath": "minunavailable",
          "children": [
            {
              "name": "Fossicking areas",
              "tocPath": "minfossicking",
              "serviceId": "mineadminareas",
              "layerId": 11
            }
          ]
        },
        {
          "name": "Constrained lands",
          "tocPath": "minconstrained",
          "children": [
            {
              "name": "Restricted areas",
              "tocPath": "minrestricted",
              "serviceId": "mineadminareas",
              "layerId": 13,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Restricted%20areas%20-%20Queensland%22"
            },
            {
              "name": "Restricted area 384 - urban",
              "tocPath": "minrestrictedurban",
              "serviceId": "mineadminareas",
              "layerId": 14,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Restricted%20areas%20-%20Queensland%22"
            },
            {
              "name": "Designated fossicking land",
              "tocPath": "mindesfossicking",
              "serviceId": "mineadminareas",
              "layerId": 15,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Queensland Mines Administrative Areas Web Map Service%22"
            }
          ]
        }
      ]
    },
    {
      "name": "NSW administrative boundaries",
      "tocPath": "nswAdmin",
      "children": [
        {
          "name": "Suburb",
          "tocPath": "nswsub",
          "serviceId": "nswAdmin",
          "layerId": 0,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        },
        {
          "name": "Local government area",
          "tocPath": "nswlga",
          "serviceId": "nswAdmin",
          "layerId": 1,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        },
        {
          "name": "State electoral district",
          "tocPath": "nswstateel",
          "serviceId": "nswAdmin",
          "layerId": 2,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        },
        {
          "name": "County",
          "tocPath": "nswcount",
          "serviceId": "nswAdmin",
          "layerId": 3,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        },
        {
          "name": "Parish",
          "tocPath": "nswparish",
          "serviceId": "nswAdmin",
          "layerId": 4,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        },
        {
          "name": "State forest",
          "tocPath": "nswstateforest",
          "serviceId": "nswAdmin",
          "layerId": 5,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        },
        {
          "name": "National parks",
          "tocPath": "nswsnatpark",
          "serviceId": "nswAdmin",
          "layerId": 6,
          "qspatialUrl": "http://sdi.nsw.gov.au/catalog/search/resource/details.page?uuid=%7BB9C44FA7-CF3E-4EBC-A226-07781514E917%7D"
        }
      ]
    },
    {
      "name": "Postcode",
      "tocPath": "postcodes",
      "serviceId": "adminbdy",
      "layerId": 3,
      "qspatialUrl": "https://www.psma.com.au/?product=postcode-boundaries"
    },
    {
      "name": "State border",
      "tocPath": "stateborder",
      "serviceId": "adminbdy",
      "layerId": 0,
      "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Coastline%20and%20State%20Border%20-%20Queensland%22"
    },
    {
      "name": "State government departments internal districts and regions",
      "tocPath": "sgidr",
      "children": [
        {
          "name": "Transport and Main Roads district",
          "tocPath": "tmrd",
          "serviceId": "stateroad",
          "layerId": 7,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Internal district boundaries - Department of Transport and Main Roads%22"
        },
        {
          "name": "Transport and Main Roads region",
          "tocPath": "tmrr",
          "serviceId": "stateroad",
          "layerId": 8,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Internal region boundaries - Department of Transport and Main Roads - Queensland%22"
        }
      ]
    }
  ]
}