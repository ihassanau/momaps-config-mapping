{
  "name": "Inland waters",
  "descriptionImage": "../images/inland-waters.jpg",
  "information": "\nThe water theme provides information where water collects and flows on and below the Earth&#39s surface.  Information available includes&#58 drainage boundaries, watercourse, water features, groundwater, \nsurface water monitoring, water plans.\n",
  "expanded": false,
  "tocPath": "inland",
  "children": [
    {
      "name": "Drainage boundaries",
      "tocPath": "drainbound",
      "children": [
        {
          "name": "Drainage divisions",
          "tocPath": "div",
          "serviceId": "drainage",
          "layerId": 0,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Drainage%20divisions%20Queensland%22"
        },
        {
          "name": "Drainage basins",
          "tocPath": "basin",
          "serviceId": "drainage",
          "layerId": 1,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Drainage%20basinss%20Queensland%22"
        },
        {
          "name": "Drainage basin sub-area",
          "tocPath": "subbasins",
          "serviceId": "drainage",
          "layerId": 2,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Drainage%20basin%20sub-area%20Queensland%22"
        }
      ]
    },
    {
      "name": "Watercourse",
      "tocPath": "waterc",
      "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=title:(%22Major%20watercourse%20lines%20-%20Queensland%22%20%22Watercourse%20areas%20-%20Queensland%22%20%20%22Lakes%20-%20Queensland%22%20%22Reservoirs%20-%20Queensland%22%20%22Canal%20areas%20-%20Queensland%22%20%22Canal%20lines%20-%20Queensland%22)",
      "hiddenChildren": [
        {
          "name": "Watercoursescache",
          "tocPath": "waterccache",
          "serviceId": "watercoursescache"
        },
        {
          "name": "Watercourse line",
          "tocPath": "waterline",
          "serviceId": "watercourses",
          "layerId": 13
        },
        {
          "name": "Watercourse area",
          "tocPath": "waterarea",
          "serviceId": "watercourses",
          "layerId": 14
        },
        {
          "name": "Water area edge",
          "tocPath": "waterareaed",
          "serviceId": "watercourses",
          "layerId": 15
        },
        {
          "name": "Coastline",
          "tocPath": "watercoast",
          "serviceId": "watercourses",
          "layerId": 4
        },
        {
          "name": "Lakes",
          "tocPath": "waterlake",
          "serviceId": "watercourses",
          "layerId": 7
        },
        {
          "name": "Reservoirs",
          "tocPath": "waterreservior",
          "serviceId": "watercourses",
          "layerId": 8
        },
        {
          "name": "Canal lines",
          "tocPath": "canalline",
          "serviceId": "watercourses",
          "layerId": 10
        },
        {
          "name": "Canal areas",
          "tocPath": "canalarea",
          "serviceId": "watercourses",
          "layerId": 11
        }
      ]
    },
    {
      "name": "Water feature",
      "tocPath": "waterf",
      "children": [
        {
          "name": "Dam",
          "tocPath": "dam",
          "serviceId": "watercourses",
          "layerId": 0,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20storage%20points%20-%20Queensland%22"
        },
        {
          "name": "Rockpool or rockhole",
          "tocPath": "waterrock",
          "serviceId": "watercourses",
          "layerId": 1,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Waterpoints%20-%20Queensland%22"
        },
        {
          "name": "Waterhole",
          "tocPath": "waterhole",
          "serviceId": "watercourses",
          "layerId": 2,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Waterpoints%20-%20Queensland%22"
        },
        {
          "name": "Waterfall",
          "tocPath": "waterfalls",
          "serviceId": "watercourses",
          "layerId": 3,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Waterfalls%20-%20Queensland%22"
        },
        {
          "name": "Flat or swamp",
          "tocPath": "swamp",
          "serviceId": "watercourses",
          "layerId": 5,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Flats%20-%20Queensland%22"
        },
        {
          "name": "Pondage area",
          "tocPath": "pondage",
          "serviceId": "watercourses",
          "layerId": 6,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Pondage%20-%20Queensland%22"
        }
      ]
    },
    {
      "name": "Groundwater",
      "tocPath": "ground",
      "children": [
        {
          "name": "Registered water bores (DNRM and private)",
          "serviceId": "watermonitoring",
          "tocPath": "regbore",
          "layerId": 1
        },
        {
          "name": "Groundwater monitoring",
          "tocPath": "groundmon",
          "children": [
            {
              "name": "Current water level water monitoring bores",
              "serviceId": "watermonitoring",
              "tocPath": "currentwatermon",
              "layerId": 3
            },
            {
              "name": "Past water monitoring and investigation bores",
              "tocPath": "pastwatermon",
              "serviceId": "watermonitoring",
              "layerId": 4
            },
            {
              "name": "Water monitoring bores with near real time data",
              "tocPath": "realtimewatermon",
              "serviceId": "watermonitoring",
              "layerId": 5
            },
            {
              "name": "All mine monitoring water bores (DNRM and private)",
              "tocPath": "allwatermon",
              "serviceId": "watermonitoring",
              "layerId": 6
            },
            {
              "name": "Mine monitoring water bores with water levels (DNRM and private)",
              "tocPath": "minewatermon",
              "serviceId": "watermonitoring",
              "layerId": 7
            },
            {
              "name": "CSG monitoring",
              "tocPath": "csgmon",
              "children": [
                {
                  "name": "Surat CMA underground water impact report monitoring bores (DNRM and private)",
                  "tocPath": "suratcma",
                  "serviceId": "watermonitoring",
                  "layerId": 9
                },
                {
                  "name": "CSG online monitoring bores (DNRM and private)",
                  "tocPath": "csgonline",
                  "serviceId": "watermonitoring",
                  "layerId": 10
                },
                {
                  "name": "CSG net monitoring bores (private)",
                  "tocPath": "csgnet",
                  "serviceId": "watermonitoring",
                  "layerId": 11
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Surface water monitoring",
      "tocPath": "surfwater",
      "children": [
        {
          "name": "Open gauging stations",
          "serviceId": "watermonitoring",
          "tocPath": "surfopengaug",
          "layerId": 13
        },
        {
          "name": "Closed gauging stations",
          "serviceId": "watermonitoring",
          "tocPath": "surfclosedgaug",
          "layerId": 14
        }
      ]
    },
    {
      "name": "Water plans",
      "tocPath": "wrps",
      "children": [
        {
          "name": "Water plan areas",
          "tocPath": "wrpsa",
          "serviceId": "watermanagement",
          "layerId": 5,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=title:(%22Water resource plan areas Queensland%22)"
        },
        {
          "name": "Barron",
          "tocPath": "wrpbarron",
          "children": [
            {
              "name": "Barron nodes",
              "tocPath": "wrpbarnode",
              "serviceId": "watermanagement",
              "layerId": 7,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water resource plan - Barron 2002 - s09 nodes%22"
            },
            {
              "name": "Barron subcatchments",
              "tocPath": "wrpbarsubcatch",
              "serviceId": "watermanagement",
              "layerId": 8,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water resource plan - Barron 2002 - s07 subcatchments%22"
            },
            {
              "name": "Barron groundwater management areas (GMA)",
              "tocPath": "wrpbargma",
              "serviceId": "watermanagement",
              "layerId": 9,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water resource plan - Barron 2002 - s05 groundwater management areas%22"
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpbarrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Atherton GMA zones",
                  "tocPath": "wrpbarath",
                  "serviceId": "watermanagement",
                  "layerId": 11,
                  "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Resource operations plan - Barron 2005 - s07 Atherton groundwater management area zones%22"
                },
                {
                  "name": "Barron supplemented surface water zones",
                  "tocPath": "wrpbarsupzon",
                  "serviceId": "watermanagement",
                  "layerId": 12,
                  "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Resource operations plan - Barron 2005 - s07 supplemented surface water zones%22"
                },
                {
                  "name": "Barron unsupplemented surface water zones",
                  "tocPath": "wrpbarunsupzon",
                  "serviceId": "watermanagement",
                  "layerId": 13,
                  "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Resource operations plan - Barron 2005 - s07 unsupplemented surface water allocation zones%22"
                }
              ]
            }
          ]
        },
        {
          "name": "Border rivers",
          "tocPath": "wrpbor",
          "children": [
            {
              "name": "Border rivers nodes",
              "tocPath": "wrpbornode",
              "serviceId": "watermanagement",
              "layerId": 15
            },
            {
              "name": "Border rivers subcatchments",
              "tocPath": "wrpborsub",
              "serviceId": "watermanagement",
              "layerId": 16
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpborrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Border rivers water management areas",
                  "tocPath": "wrpborwma",
                  "serviceId": "watermanagement",
                  "layerId": 18
                },
                {
                  "name": "Border rivers supplemented surface water zones",
                  "tocPath": "wrpborsupzon",
                  "serviceId": "watermanagement",
                  "layerId": 19
                },
                {
                  "name": "Stanthorpe water management area",
                  "tocPath": "wrpborstanwma",
                  "serviceId": "watermanagement",
                  "layerId": 20
                },
                {
                  "name": "Border rivers unsupplemented surface water zones",
                  "tocPath": "wrpborunsupzon",
                  "serviceId": "watermanagement",
                  "layerId": 21
                },
                {
                  "name": "Stanthorpe water management area subcatchments",
                  "tocPath": "wrpborstanwmasub",
                  "serviceId": "watermanagement",
                  "layerId": 22
                }
              ]
            }
          ]
        },
        {
          "name": "Burdekin basin",
          "tocPath": "wrpburd",
          "children": [
            {
              "name": "Burdekin basin nodes",
              "tocPath": "wrpburdnode",
              "serviceId": "watermanagement",
              "layerId": 24
            },
            {
              "name": "Burdekin basin subcatchments",
              "tocPath": "wrpburdsub",
              "serviceId": "watermanagement",
              "layerId": 25
            },
            {
              "name": "Giru benefited groundwater area",
              "tocPath": "wrpburdgiru",
              "serviceId": "watermanagement",
              "layerId": 26
            },
            {
              "name": "Burdekin basin water management area",
              "tocPath": "wrpburdwma",
              "serviceId": "watermanagement",
              "layerId": 27
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpburdrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Burdekin basin zones",
                  "tocPath": "wrpburdzones",
                  "serviceId": "watermanagement",
                  "layerId": 29
                }
              ]
            }
          ]
        },
        {
          "name": "Burnett basin",
          "tocPath": "wrpburn",
          "children": [
            {
              "name": "Burnett basin nodes - surface water",
              "tocPath": "wrpburnsurnode",
              "serviceId": "watermanagement",
              "layerId": 31,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s11%20nodes%20-%20surface%20water%22"
            },
            {
              "name": "Burnett basin nodes - groundwater",
              "tocPath": "wrpburngnode",
              "serviceId": "watermanagement",
              "layerId": 32,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s11%20nodes%20-%20groundwater%22"
            },
            {
              "name": "Coastal Burnett overland flow area",
              "tocPath": "wrpburncost",
              "serviceId": "watermanagement",
              "layerId": 33,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s06%20Coastal%20Burnett%20overland%20flow%20area%22"
            },
            {
              "name": "Burnett basin groundwater management areas (GMA)",
              "tocPath": "wrpburngma",
              "serviceId": "watermanagement",
              "layerId": 34,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s07%20groundwater%20management%20areas%22"
            },
            {
              "name": "Upper Burnett GMA sub-areas",
              "tocPath": "wrpupburngma",
              "serviceId": "watermanagement",
              "layerId": 35,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s08%20groundwater%20units%20sub-areas%20-%20Upper%20Burnett%22"
            },
            {
              "name": "Coastal Burnett GMA unit 1 sub-areas",
              "tocPath": "wrpcburngma1",
              "serviceId": "watermanagement",
              "layerId": 36,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s08%20groundwater%20units%20sub-areas%20-%20Coastal%20Burnett%20unit%201%22"
            },
            {
              "name": "Coastal Burnett GMA unit 2 sub-areas",
              "tocPath": "wrpcburngma2",
              "serviceId": "watermanagement",
              "layerId": 37,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s08%20groundwater%20units%20sub-areas%20-%20Coastal%20Burnett%20unit%202%22"
            },
            {
              "name": "Burnett basin nodes subcatchment areas",
              "tocPath": "wrpburnsub",
              "serviceId": "watermanagement",
              "layerId": 38,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Burnett%20Basin%202014%20-%20s05%20subcatchment%20areas%22"
            }
          ]
        },
        {
          "name": "Condamine and Balonne",
          "tocPath": "wrpcond",
          "children": [
            {
              "name": "Condamine and Balonne nodes",
              "tocPath": "wrpcondnode",
              "serviceId": "watermanagement",
              "layerId": 40
            },
            {
              "name": "Condamine and Balonne groundwater management areas (GMA)",
              "tocPath": "wrpcondgma",
              "serviceId": "watermanagement",
              "layerId": 41
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpcondrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Condamine and Balonne unsupplemented surface water area zones",
                  "tocPath": "wrpcondunsuparea",
                  "serviceId": "watermanagement",
                  "layerId": 43
                },
                {
                  "name": "Condamine and Balonne unsupplemented surface water zones - line",
                  "tocPath": "wrpcondunsupline",
                  "serviceId": "watermanagement",
                  "layerId": 44
                },
                {
                  "name": "Condamine and Balonne supplemented surface water zones",
                  "tocPath": "wrpcondsupzone",
                  "serviceId": "watermanagement",
                  "layerId": 45
                }
              ]
            }
          ]
        },
        {
          "name": "Great Artesian Basin",
          "tocPath": "wrpgab",
          "children": [
            {
              "name": "Great Artesian Basin plan area",
              "tocPath": "wrpgabplan",
              "serviceId": "watermanagement",
              "layerId": 47,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=title:(%22Water resource plan Great Artesian Basin 2006 plan area%22)"
            },
            {
              "name": "Great Artesian Basin management areas (GMA)",
              "tocPath": "wrpgabma",
              "serviceId": "watermanagement",
              "layerId": 48,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=title:(%22Water resource plan Great Artesian Basin 2006 management areas%22)"
            }
          ]
        },
        {
          "name": "Gulf",
          "tocPath": "wrpgulf",
          "children": [
            {
              "name": "Gulf nodes",
              "tocPath": "wrpgulfnode",
              "serviceId": "watermanagement",
              "layerId": 50
            },
            {
              "name": "Gulf prescribed watercourse",
              "tocPath": "wrpgulfprescr",
              "serviceId": "watermanagement",
              "layerId": 51
            },
            {
              "name": "Gulf catchment areas",
              "tocPath": "wrpgulfcat",
              "serviceId": "watermanagement",
              "layerId": 52
            },
            {
              "name": "Gulf subcatchment areas",
              "tocPath": "wrpgulfsubcat",
              "serviceId": "watermanagement",
              "layerId": 53
            },
            {
              "name": "Gulf groundwater management areas (GMA)",
              "tocPath": "wrpgulfgma",
              "serviceId": "watermanagement",
              "layerId": 54
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpgulfrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Gulf water management areas",
                  "tocPath": "wrpgulfwma",
                  "serviceId": "watermanagement",
                  "layerId": 56
                },
                {
                  "name": "Gulf zones",
                  "tocPath": "wrpgulfzones",
                  "serviceId": "watermanagement",
                  "layerId": 57
                }
              ]
            }
          ]
        },
        {
          "name": "Mitchell",
          "tocPath": "wrpmit",
          "children": [
            {
              "name": "Mitchell nodes",
              "tocPath": "wrpmitnode",
              "serviceId": "watermanagement",
              "layerId": 59
            },
            {
              "name": "Mitchell prescribed watercourse",
              "tocPath": "wrpmitpres",
              "serviceId": "watermanagement",
              "layerId": 60
            },
            {
              "name": "Mitchell subcatchment areas",
              "tocPath": "wrpmitsubcat",
              "serviceId": "watermanagement",
              "layerId": 61
            },
            {
              "name": "Mitchell groundwater management areas (GMA)",
              "tocPath": "wrpmitgma",
              "serviceId": "watermanagement",
              "layerId": 62
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpmitrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Mitchell seasonal water assignment zones",
                  "tocPath": "wrpmitseas",
                  "serviceId": "watermanagement",
                  "layerId": 64
                }
              ]
            }
          ]
        },
        {
          "name": "Moonie",
          "tocPath": "wrpmoon",
          "children": [
            {
              "name": "Moonie nodes",
              "tocPath": "wrpmoonnode",
              "serviceId": "watermanagement",
              "layerId": 66
            },
            {
              "name": "Moonie groundwater management areas (GMA)",
              "tocPath": "wrpmoongma",
              "serviceId": "watermanagement",
              "layerId": 67
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpmoonrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Moonie zones",
                  "tocPath": "wrpmoonzones",
                  "serviceId": "watermanagement",
                  "layerId": 69
                }
              ]
            }
          ]
        },
        {
          "name": "Pioneer Valley",
          "tocPath": "wrppio",
          "children": [
            {
              "name": "Pioneer Valley nodes - surface water",
              "tocPath": "wrppiosurnode",
              "serviceId": "watermanagement",
              "layerId": 71
            },
            {
              "name": "Pioneer Valley nodes - groundwater",
              "tocPath": "wrppiogronode",
              "serviceId": "watermanagement",
              "layerId": 72
            },
            {
              "name": "Pioneer Valley seawater intrusion coastline",
              "tocPath": "wrppioseaco",
              "serviceId": "watermanagement",
              "layerId": 73
            },
            {
              "name": "Pioneer Valley seawater intrusion baseline",
              "tocPath": "wrppioseaba",
              "serviceId": "watermanagement",
              "layerId": 74
            },
            {
              "name": "Pioneer Valley subcatchment areas",
              "tocPath": "wrppiosub",
              "serviceId": "watermanagement",
              "layerId": 75
            },
            {
              "name": "Pioneer Valley GMA & sub areas",
              "tocPath": "wrppiogma",
              "serviceId": "watermanagement",
              "layerId": 76
            },
            {
              "name": "Pioneer Valley domestic areas",
              "tocPath": "wrppiodom",
              "serviceId": "watermanagement",
              "layerId": 77
            },
            {
              "name": "Pioneer Valley watercourse area",
              "tocPath": "wrppiowat",
              "serviceId": "watermanagement",
              "layerId": 78
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrppionrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Pioneer Valley unsupplemented zones",
                  "tocPath": "wrppiounsup",
                  "serviceId": "watermanagement",
                  "layerId": 80
                },
                {
                  "name": "Pioneer Valley GMA zones",
                  "tocPath": "wrppiogmazone",
                  "serviceId": "watermanagement",
                  "layerId": 81
                }
              ]
            }
          ]
        },
        {
          "name": "Warrego Paroo Bulloo Nebine",
          "tocPath": "wrpwpbn",
          "children": [
            {
              "name": "Warrego Paroo Bulloo Nebine nodes",
              "tocPath": "wrpwpbnnode",
              "serviceId": "watermanagement",
              "layerId": 83,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20Warrego%20Paroo%20Bulloo%20Nebine%202016%20s08%20nodes%22"
            },
            {
              "name": "Warrego Paroo Bulloo Nebine catchment areas",
              "tocPath": "wrpwpbncat",
              "serviceId": "watermanagement",
              "layerId": 84,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20Warrego%20Paroo%20Bulloo%20Nebine%202016%20s05%20catchment%20areas%22"
            },
            {
              "name": "Warrego Paroo Bulloo Nebine groundwater management areas (GMA)",
              "tocPath": "wrpwpbngma",
              "serviceId": "watermanagement",
              "layerId": 85,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20Warrego%20Paroo%20Bulloo%20Nebine%202016%20s06%20groundwater%20management%20area%22"
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpwpbnnrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Warrego Paroo Bulloo Nebine water management areas",
                  "tocPath": "wrpwpbnpwma",
                  "serviceId": "watermanagement",
                  "layerId": 87,
                  "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=title:(%22Resource operations plan Warrego Paroo Bulloo Nebine 2006 s06 water management areas%22)"
                },
                {
                  "name": "Warrego Paroo Bulloo Nebine resource operations licence holder",
                  "tocPath": "wrpwpbnplicen",
                  "serviceId": "watermanagement",
                  "layerId": 88,
                  "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=title:(%22Resource operations plan Warrego Paroo Bulloo Nebine 2006 s05 resource operations licence holder%22)"
                },
                {
                  "name": "Warrego Paroo Bulloo Nebine zones",
                  "tocPath": "wrpwpbnpzones",
                  "serviceId": "watermanagement",
                  "layerId": 89,
                  "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Resource%20operations%20plan%20Warrego%20Paroo%20Bulloo%20Nebine%202006%20s07%20resource%20operations%20plan%20zone%22"
                }
              ]
            }
          ]
        },
        {
          "name": "Wet Tropics",
          "tocPath": "wrpwt",
          "children": [
            {
              "name": "Wet Tropics nodes",
              "tocPath": "wrpwtnode",
              "serviceId": "watermanagement",
              "layerId": 91
            },
            {
              "name": "Wet Tropics groundwater management areas (GMA)",
              "tocPath": "wrpwtgma",
              "serviceId": "watermanagement",
              "layerId": 92,
              "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Water%20resource%20plan%20-%20Wet%20Tropics%202013%20-%20groundwater%20management%20areas%22"
            },
            {
              "name": "Wet Tropics catchment areas",
              "tocPath": "wrpwtcat",
              "serviceId": "watermanagement",
              "layerId": 93
            },
            {
              "name": "Wet Tropics subcatchment areas",
              "tocPath": "wrpwtsubcat",
              "serviceId": "watermanagement",
              "layerId": 94
            },
            {
              "name": "Resource operations plan",
              "tocPath": "wrpwtrop",
              "serviceId": "watermanagement",
              "children": [
                {
                  "name": "Wet Tropics water management areas",
                  "tocPath": "wrpwtwma",
                  "serviceId": "watermanagement",
                  "layerId": 96
                },
                {
                  "name": "Wet Tropics water management area zones",
                  "tocPath": "wrpwtwmazon",
                  "serviceId": "watermanagement",
                  "layerId": 97
                },
                {
                  "name": "Wet Tropics GMA zones",
                  "tocPath": "wrpwtgmazon",
                  "serviceId": "watermanagement",
                  "layerId": 98
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Watercourse identification map (Water Act 2000)",
      "tocPath": "ident",
      "children": [
        {
          "name": "Watercourse (defined by Water Act 2000)",
          "tocPath": "course",
          "serviceId": "wim",
          "layerId": 1,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Watercourse%20identification%20map%20-%20watercourse%20-%20Queensland%22"
        },
        {
          "name": "Drainage features (defined by Water Act 2000)",
          "tocPath": "drainage",
          "serviceId": "wim",
          "layerId": 2,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Watercourse%20identification%20map%20-%20drainage%20feature%20-%20Queensland%22"
        },
        {
          "name": "Downstream Limit (defined by Water Act 2000)",
          "tocPath": "downstream",
          "serviceId": "wim",
          "layerId": 3,
          "qspatialUrl": "http://qldspatial.information.qld.gov.au/catalogue/custom/search.page?q=%22Watercourse%20identification%20map%20-%20downstream%20limit%20-%20Queensland%22"
        }
      ]
    }
  ]
}